FROM tomcat:latest

LABEL maintainer=”demo@test.com”

ADD ./target/*.war /usr/local/tomcat/webapps/

EXPOSE 8080
